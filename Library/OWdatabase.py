#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3

from OWresource import OWresource

OWdatabase = sqlite3.connect( OWresource[ 'sql_data' ] )

def log_query( query ):

    #pass
    print( query )

OWdatabase.set_trace_callback( log_query )

####################################################################################
#   UNIT TESTS
#
if __name__ == "__main__":

    for player in OWdatabase.execute( 'SELECT * FROM players' ).fetchall():

        print( player )
