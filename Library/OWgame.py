#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWdatabase import OWdatabase as OWdb
from OWplayer   import OWplayer

RESULT_WRONG = -1
RESULT_OK    =  0
RESULT_SURE  =  1

RESULTS = ( RESULT_WRONG, RESULT_OK, RESULT_SURE )

class OWgame():

    def __init__( self, game_id = "unknown" ):

        self.id = game_id

    def update_results( self, player_id, meaning_id, result ):

        assert( result in RESULTS )

        OWdb.execute(
            "INSERT INTO results( player, game_id, meaning_id, value ) VALUES" +
            "( %d, '%s', '%s', %d )" % ( player_id, self.id, meaning_id, result )
        )
