#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource

default_lang = 'lo'

#   Load translations
#
lango_lo = {}

with open( OWresource[ 'i18n_lo' ], 'r' ) as lang_lo_file:

    lango_lo = eval( lang_lo_file.read() )

def T( text, target_language = default_lang ):

    try:

        if target_language == 'lo':

            return lang_lo.translations[ text ]

    except KeyError:

        pass

    return text
