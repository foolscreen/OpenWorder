#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource
from OWdatabase import OWdatabase as OWdb

from kivy.uix.button import Button

class OWword():

    #   Interface
    #
    def __init__( self, word_id ):

        self.id, self.lang, self.level, self.text = OWdb.execute(
            "SELECT * FROM words WHERE id = %d;" % word_id ).fetchone()

        self.meanings = None

    def meanings( self ):

        return [ OWmeaning( data ) for data[ 0 ] in OWdb.execute(

            'SELECT id FROM meanings WHERE '
        ).fetchall() ]

    #   GUI
    #
    def as_widget( self ):

        font_name = OWresource[ 'language_font' ][ self.lang ]

        button = Button( text = self.text, font_name = font_name )

        if self.lang == 'lo':

            button.padding = ( 0, button.padding[ 1 ] )

        return button

####################################################################################
#   UNIT TESTS
#
if __name__ == "__main__":

    for wid in OWdb.execute('SELECT id FROM words').fetchall():

        print( OWword( wid ).text )
