#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource
from OWdatabase import OWdatabase as OWdb
from OWplayer   import OWplayer

from kivy.uix.gridlayout import GridLayout
from kivy.uix.image      import Image
from kivy.uix.textinput  import TextInput

class OWlogin( GridLayout ):

    def __init__( self ):

        super( OWlogin, self ).__init__( cols = 2 )

        name_icon = Image( source = OWresource[ 'icon' ][ 'username' ], size_hint = ( 0.25, 1.0 ) )
        pwd_icon  = Image( source = OWresource[ 'icon' ][ 'password' ], size_hint = ( 0.25, 1.0 ) )

        self.namebox = TextInput( font_name = OWresource[ 'language_font' ][ None ] )
        self.pwdbox  = TextInput( font_name = OWresource[ 'language_font' ][ None ] )

        self.add_widget( name_icon    )
        self.add_widget( self.namebox )
        self.add_widget( pwd_icon     )
        self.add_widget( self.pwdbox  )

        self.namebox.focus = True

        def pwd_focus( source ): self.pwdbox.focus = True

        self.namebox.bind( on_text_validate = pwd_focus )

    def authenticate( self ):

        try:

            player_id, correct_password = OWdb.execute(
                "SELECT id, pwd FROM players WHERE name = '%s'" % self.namebox.text.lower() ).fetchone()

        except TypeError:

            correct_password = None

        if correct_password is not None and self.pwdbox.text.lower() == correct_password.lower():

            OWresource[ 'sound' ][ 'correct' ].play()

            return OWplayer( player_id )

        else:

            OWresource[ 'sound' ][ 'wrong' ].play()

            self.namebox.focus = True

            return None
