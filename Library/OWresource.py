#!/usr/bin/python3
# -*- coding: utf-8 -*-

from kivy.core.audio import SoundLoader
from kivy.core.text  import LabelBase

PATH_APP    = '/home/bengo/WORK/School/OpenWorder/'
PATH_FONTS  = "/usr/share/fonts/truetype/"
PATH_ICONS  = PATH_APP + 'Icons/'
PATH_SOUNDS = PATH_APP + 'Sounds/'

OWresource = {

    'language_font': {
        None: 'DejaVu',
        'en': 'DejaVu',
        'lo': 'Saysettha'
    },

    'sql_data':        PATH_APP + 'openworder.db',
    'sentence_speech': PATH_APP + 'Audio/sentences/%d.mp3',

    'font_name': {

        'DejaVu': [
            PATH_FONTS + 'ttf-dejavu/DejaVuSans.ttf',
            PATH_FONTS + 'ttf-dejavu/DejaVuSans-Bold.ttf'
        ],

        'Saysettha': [
            PATH_FONTS + 'lao/saysettha_ot.ttf',
            PATH_FONTS + 'lao/saysettha_ot.ttf'
        ]
    },

    'icon': {
        'username':   PATH_ICONS + 'system-users.png',
        'password':   PATH_ICONS + 'preferences-desktop-cryptography.png',
        'openworder': PATH_ICONS + 'kthesaurus.png'
    },

    'sound': {
        'correct': SoundLoader.load( PATH_SOUNDS + 'LTTP_Get_HeartPiece.wav' ),
        'wrong':   SoundLoader.load( PATH_SOUNDS + 'LTTP_Link_Fall.wav'      )
    }
}

for font_name, fonts in OWresource[ 'font_name' ].items():

    LabelBase.register( name = font_name, fn_regular = fonts[ 0 ], fn_bold = fonts[ 1 ] )
