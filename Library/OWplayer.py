#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource

from OWdatabase import OWdatabase as OWdb
from OWword     import OWword

from kivy.core.audio      import SoundLoader
from kivy.uix.stacklayout import StackLayout

class OWplayer():

    def __init__( self, player_id ):

        self.id, self.name, self.pwd, self.native, self.level = OWdb.execute(
            "SELECT * FROM players WHERE id = %d;" % player_id ).fetchone()
