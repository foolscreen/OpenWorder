#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource

from OWdatabase import OWdatabase as OWdb
from OWword     import OWword

from kivy.core.audio      import SoundLoader
from kivy.uix.stacklayout import StackLayout

class OWmeaning():

    def __init__( self, meaning_id ):

        self.id, word_id, self.grammar, definition_id = OWdb.execute(
            "SELECT * FROM meanings WHERE id = %d;" % meaning_id ).fetchone()

        self.word = OWword( word_id )

        self.definitions = {}

        def_en, def_lo = OWdb.execute(
            "SELECT text_en, text_lo FROM definitions " +
            "WHERE id = %d;" % definition_id ).fetchone()

        self.definitions[ 'en' ] = def_en
        self.definitions[ 'lo' ] = def_lo