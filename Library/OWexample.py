#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWdatabase import OWdatabase as OWdb
from OWsentence import OWsentence
from OWmeaning  import OWmeaning

class OWexample():

    def __init__( self, example_id ):

        self.id, sid, self.word_position, wmid = OWdb.execute(
            "SELECT id, sentence, word_position, meaning " +
            "FROM examples WHERE id = %d;" % example_id ).fetchone()
        print( 'XXX', example_id, self.word_position )
        self.sentence   = OWsentence( sid  )
        self.meaning    = OWmeaning ( wmid )

####################################################################################
#   UNIT TESTS
#
if __name__ == "__main__":

    for eid in OWdb.execute( 'SELECT id FROM examples' ).fetchall():

        print( OWexample( eid ).sentence.text )
