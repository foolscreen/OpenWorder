#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource

from OWdatabase import OWdatabase as OWdb
from OWword     import OWword

from kivy.core.audio      import SoundLoader
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput   import TextInput
from kivy.core.text       import Label as CoreLabel

class OWsentence():

    def __init__( self, sentence_id ):

        self.id, self.lang, self.level = OWdb.execute(
            "SELECT * FROM sentences WHERE id = %d;" % sentence_id ).fetchone()

        self.words = [

            OWword( wid[ 0 ] )

            for wid in OWdb.execute(
                "SELECT word FROM sentence_words WHERE sentence = %d ORDER BY position;" % self.id ).fetchall()
        ]

        self.text = u' '.join( [ w.text for w in self.words ] )

        self.speech = SoundLoader.load( OWresource[ 'sentence_speech' ] % self.id )

    def as_widget( self, hole = None ):

        assert( hole is None or ( type( hole ) == int and hole >= 0 ) )

        layout = StackLayout()

        wpos = 0

        for word in self.words:

            if wpos == hole:

                solution_ui = CoreLabel(
                    text      = word.text,
                    font_size = 48,
                    font_name = OWresource[ 'language_font'][ word.lang ] )

                solution_ui.refresh()

                self.answer_ui = TextInput( size_hint = ( None, None ) )

                self.answer_ui.width  = solution_ui.width  + 20
                self.answer_ui.height = solution_ui.height + 20

                layout.add_widget( self.answer_ui )

            else:

                button = word.as_widget()

                button.bind( on_press = lambda args: self.speech.play() )

                layout.add_widget( button )

            wpos += 1

        return layout

####################################################################################
#   UNIT TESTS
#
if __name__ == "__main__":

    for sid in OWdb.execute( 'SELECT id FROM sentences' ).fetchall():

        print( OWsentence( sid ).text )
