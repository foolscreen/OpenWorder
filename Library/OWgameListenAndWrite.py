#!/usr/bin/python3
# -*- coding: utf-8 -*-

from OWresource import OWresource
from OWdatabase import OWdatabase as OWdb
from OWgame     import OWgame
from OWexample  import OWexample

import random

def get_random_example():

    random_id = random.randint( 1, 56 )

    return OWexample( random_id )

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button      import Button
from kivy.clock      import Clock

class ListenAndWrite( OWgame, FloatLayout ):

    def __init__( self ):

        OWgame.__init__( self, 'ListenAndWrite' )
        FloatLayout.__init__( self )

        self.next()

    def focus( self, args ):

        self.example.sentence.answer_ui.focus = True

    def next( self ):

        self.attempt = 1

        self.clear_widgets()

        self.example = get_random_example()

        sentence = self.example.sentence

        self.add_widget( sentence.as_widget( hole = self.example.word_position ) )

        Clock.schedule_interval( self.focus, 0.33 )

        self.example.sentence.answer_ui.bind( on_text_validate = self.validate )

        self.example.sentence.speech.play()

    def validate( self, answer ):

        if self.example.sentence.answer_ui.text.lower() == self.example.meaning.word.text.lower():

            OWresource[ 'sound' ][ 'correct' ].play()

            self.answer()

        else:

            OWresource[ 'sound' ][ 'wrong' ].play()

            if self.attempt == 3:

                self.answer()

            self.attempt += 1

    def answer( self ):

        self.clear_widgets()

        self.add_widget( self.example.sentence.as_widget() )

        button = Button( text = '->', on_press = lambda a: self.next() )

        self.add_widget( button )

        self.example.sentence.speech.play()

        Clock.unschedule( self.focus )

        del self.example
