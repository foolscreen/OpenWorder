#!/usr/bin/python3
# -*- coding: utf-8 -*-

############################################################################
#	OPENWORDER MODULES
#
import sys

sys.path.insert( 0, 'Library' )

from OWlogin                import OWlogin
from OWresource             import OWresource
from OWgameListenAndWrite   import ListenAndWrite

############################################################################
#	PYTHON AND KIVY MODULES
#
from kivy.app				import App
from kivy.lang				import Builder

############################################################################
#	SET FONT SIZE
#
FONT_BIG	= 48
FONT_AVG	= 24
FONT_SMALL	= 12

############################################################################
#	APPLICATION
#
class OpenWorder( App ):

    icon = OWresource[ 'icon' ][ 'openworder' ]

    def login( self, data ):

        self.player = self.root.authenticate()

        if not self.player: return

        self.root_window.remove_widget( self.root )

        self.root = ListenAndWrite()

        self.root_window.add_widget( self.root )

    def build( self ):

        login = OWlogin()

        login.pwdbox.bind( on_text_validate = self.login )

        return login

#	Start the program
#
if __name__ == '__main__':

    #	Configure application
    #
    Builder.load_string( """

<Label, Button>:
    size_hint:			None, None
    size:				self.texture_size

<Label, Button, TextInput>:
    padding:			10, 10
    font_size:			%d

    background_color:	[ 0.0, 0.0, 0.0, 1.0 ]
    foreground_color:	[ 1.0, 1.0, 1.0, 1.0 ]

    markup:             True

<TextInput>:
    padding:            [ 10, ( self.height - self.line_height ) / 2 ]
    multiline:			False
    write_tab:			False

    """ % FONT_BIG )

    #	Run the application
    #
    OpenWorder().run()
