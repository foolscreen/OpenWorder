#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sqlite3

#	PARAMETERS
#
LANG_EN = 'en'
LANG_LO = 'lo'

LEVEL_KINDERGARTEN	= 1
LEVEL_PRIMARY		= 2
LEVEL_SECONDARY		= 3
LEVEL_UNIVERSITY	= 4

#############################################################################
#	INITIALIZE OPENWORDER DATABASE SCHEMA
#
def init_schema( db ):

    #	- Create tables
    #
    db.execute( """CREATE TABLE words
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        lang		char( 2 )		NOT NULL CHECK ( lang = 'en' OR lang = 'lo' ),
        level		integer			NOT NULL CHECK ( level >= 1 AND level <= 4  ),

        text		varchar( 40 )	UNIQUE NOT NULL CHECK ( text <> '' )
    );""" )

    db.execute( """CREATE TABLE definitions
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        text_en		text			NOT NULL CHECK ( text_en <> '' ),
        text_lo		text
    );""" )

    db.execute( """CREATE TABLE meanings
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        word		integer			REFERENCES words,
        grammar		varchar( 20 )	NOT NULL CHECK ( grammar <> '' ),

        definition	integer			REFERENCES definitions
    );""" )

    db.execute( """CREATE TABLE lists
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        parent		integer			REFERENCES lists,
        name		varchar( 40 )	NOT NULL CHECK ( name <> '' ),
        description	text
    );""" )

    db.execute( """CREATE TABLE list_words
    (
        list		integer			REFERENCES lists,
        word		integer			REFERENCES words
    );""" )

    db.execute( """CREATE TABLE sentences
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        lang		char( 2 )		NOT NULL CHECK ( lang = 'en' OR lang = 'lo' ),
        level		integer			NOT NULL CHECK ( level >= 1 AND level <= 4  )
    );""" )

    db.execute( """CREATE TABLE sentence_words
    (
        sentence	integer			REFERENCES sentences,
        word		integer			REFERENCES words,
        position    integer			NOT NULL
    );""" )

    db.execute( """CREATE TABLE examples
    (
        id				integer		PRIMARY KEY AUTOINCREMENT NOT NULL,

        sentence		integer		REFERENCES sentences,
        word_position	integer		NOT NULL,
		meaning         integer		REFERENCES meanings
    );""" )

    db.execute( """CREATE TABLE players
    (
        id			integer			PRIMARY KEY AUTOINCREMENT NOT NULL,

        name		varchar( 40 )	NOT NULL CHECK ( name <> '' ),
        pwd			varchar( 40 )	NOT NULL,
        native      char( 2 )		NOT NULL CHECK ( native = 'en' OR native = 'lo' ),
        level		integer			NOT NULL CHECK ( level >= 1 AND level <= 4  )
    );""" )

    db.execute( """CREATE TABLE results
    (
        player		integer			REFERENCES players,

        game_id		varchar( 20 )   NOT NULL CHECK ( game_id <> '' ),
        value		integer         NOT NULL CHECK ( value >= -1 AND value <= 1  ),

        datetime	varchar( 20 )   NOT NULL DEFAULT( DATETIME( 'now' ) )
    )
    """ )

    #	- Create views ( cached queries )
    #
    db.execute( "CREATE VIEW words_from_sentences AS " +
        "SELECT words.* FROM words, sentence_words " +
        "WHERE words.id = sentence_words.word " +
        "GROUP BY words.id ORDER BY words.id" )

    db.execute("CREATE VIEW words_from_examples AS " +
       "SELECT words.* FROM words, meanings, examples " +
       "WHERE words.id = meanings.word AND meanings.id = examples.meaning " +
       "GROUP BY words.id ORDER BY words.id")


#############################################################################
#	INSERT DEFAULT OPENWORDER PLAYERS
#
#	- Cake      ( Level: kindergarten )
#   - Angoon    ( Level: primary )
#	- Dtui      ( Level: secondary )
#   - Ben       ( Level: university )
#
def init_players( db ):

    db.execute(

        "INSERT INTO players( name, pwd, native, level ) VALUES" +
        "( 'cake',   '', 'lo', 1 )," +
        "( 'angoon', '', 'lo', 2 )," +
        "( 'toui',   '', 'lo', 3 )," +
        "( 'ben',    '', 'en', 4 );"
    )

#############################################################################
#	INSERT DEFAULT OPENWORDER DATA
#
#	- Cardinal Numbers in English
#	- Cardinal Numbers in Lao
#
def add_words( db, lang, level, words ):

    for word in words:

        db.execute(

            'INSERT INTO words( lang, level, text )' +
            ' VALUES ( "%s", %d, "%s" )' % ( lang, level, word )
        )

def init_data( db ):

    #	Cardinal Numbers
    #
    db.execute( 'INSERT INTO lists VALUES( 0, 0, "Root", "" )' )

    db.execute(

        'INSERT INTO lists( parent, name, description ) VALUES' +
        ' ( 0, "%s", "%s" ),' % ( 'Cardinal Numbers', '...' ) +
        ' ( 0, "%s", "%s" );' % ( 'ຫມາຽເລກ', '...' )
    )

    list_en_db_id = 1
    list_lo_db_id = 2

    numbers_en = (

        "0 1 2 3 4 5 6 7 8 9" +
        " 10 20 30 40 50 60 70 80 90" +
        " 11 12 13 14 15 16 17 18 19" +
        " 100 1000"

    ).split()

    numbers_lo = (

        "໐ ໑ ໒ ໓ ໔ ໕ ໖ ໗ ໘ ໙" +
        " ໑໐ ໒໐ ໓໐ ໔໐ ໕໐ ໖໐ ໗໐ ໘໐ ໙໐" +
        " ໑໑ ໑໒ ໑໓ ໑໔ ໑໕ ໑໖ ໑໗ ໑໘ ໑໙" +
        " ໑໐໐ ໑໐໐໐"

    ).split()

    words_en = (

        "zero one two three four five six seven eight nine" +
        " ten twenty thirty fourty fifty sixty seventy eighty ninety" +
        " eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen" +
        " hundred thousand"

    ).split()

    words_en[ -2 ] = "one hundred"
    words_en[ -1 ] = "one thousand"

    words_lo = (
        "ສູນ ນຶ່ງ ສອງ ສາມ ສີ່ ຫ້າ ຫົກ ເຈັດ ແປດ ເກົ້າ"
        " ສິບ ຊາວ ສາມສິບ ສີ່ສິບ ຫ້າສິບ ຫົກສິບ ເຈັດສິບ ແປດສິບ ເກົ້າສິບ"
        " ສິບເອັດ ສິບສອງ ສິບສາມ ສິບສີ່ ສິບຫ້າ ສິບຫົກ ສິບເຈັດ ສິບແປດ ສິບເກົ້າ"
        " ນຶ່ງຮ້ອຍ ນຶ່ງພັນ"

    ).split()

    add_words( db, LANG_EN, LEVEL_KINDERGARTEN,	numbers_en	[ : 10 ] )
    add_words( db, LANG_EN, LEVEL_PRIMARY,		numbers_en	[ 10 : ] )
    add_words( db, LANG_LO, LEVEL_KINDERGARTEN,	numbers_lo	[ : 10 ] )
    add_words( db, LANG_LO, LEVEL_PRIMARY,		numbers_lo	[ 10 : ] )
    add_words( db, LANG_EN, LEVEL_KINDERGARTEN,	words_en	[ : 10 ] )
    add_words( db, LANG_EN, LEVEL_PRIMARY,		words_en	[ 10 : ] )
    add_words( db, LANG_LO, LEVEL_KINDERGARTEN,	words_lo	[ : 10 ] )
    add_words( db, LANG_LO, LEVEL_PRIMARY,		words_lo	[ 10 : ] )

    numb_en_db_ids = 1
    numb_lo_db_ids = 31
    word_en_db_ids = 61
    word_lo_db_ids = 91

    add_words( db, LANG_EN, LEVEL_KINDERGARTEN,	u'I have no a flower flowers .'.split() )
    add_words( db, LANG_LO, LEVEL_KINDERGARTEN,	u'ຂ້ອຍ ບໍ່ ມີ ດອກໄມ້ ອັນ'.split() )

    id_I, id_have, id_no, id_a, id_flower, id_flowers, id_dot = range( 121, 128 )
    id_khoy, id_bor, id_mee, id_dokmai, id_an = range( 128, 133 )

    grammar = 'number'

    for idx in range( 30 ):

        definition_en = numbers_en[ idx ] + ', ' + words_en[ idx ]
        definition_lo = numbers_lo[ idx ] + ', ' + words_lo[ idx ]

        db.execute(

            'INSERT INTO definitions( text_en, text_lo ) VALUES' +
            ' ( "%s", "%s" )' % ( definition_en, definition_lo )
        )

        definition_db_id = idx + 1

        db.execute(

            'INSERT INTO meanings( word, grammar, definition ) VALUES' +
            ' ( %d, "%s", %d ),' % ( numb_en_db_ids + idx, grammar, definition_db_id ) +
            ' ( %d, "%s", %d ),' % ( numb_lo_db_ids + idx, grammar, definition_db_id ) +
            ' ( %d, "%s", %d ),' % ( word_en_db_ids + idx, grammar, definition_db_id ) +
            ' ( %d, "%s", %d );' % ( word_lo_db_ids + idx, grammar, definition_db_id )
        )

        wm_wo_en_db_id = idx * 4 + 3
        wm_wo_lo_db_id = idx * 4 + 4

        db.execute(

            'INSERT INTO list_words( list, word ) VALUES' +
            ' ( %d, %d ),' % ( list_en_db_id, numb_en_db_ids + idx ) +
            ' ( %d, %d ),' % ( list_en_db_id, word_en_db_ids + idx ) +
            ' ( %d, %d ),' % ( list_lo_db_id, numb_lo_db_ids + idx ) +
            ' ( %d, %d );' % ( list_lo_db_id, word_lo_db_ids + idx )
        )

        if idx == 0:
            sentence_words_en = [ id_I, id_have, id_no, id_flower, id_dot ]
            sentence_words_lo = [ id_khoy, id_bor, id_mee, id_dokmai ]
        elif idx == 1:

            sentence_words_en = [ id_I, id_have, id_a, id_flower, id_dot ]
            sentence_words_lo = [ id_khoy, id_mee, id_dokmai, id_an, word_lo_db_ids + 1 ]
        else:
            sentence_words_en = [ id_I, id_have, word_en_db_ids + idx, id_flowers, id_dot ]
            sentence_words_lo = [ id_khoy, id_mee, id_dokmai, word_lo_db_ids + idx, id_an ]

        db.execute(

            'INSERT INTO sentences( lang, level ) VALUES' +
            ' ( "%s", %d ),' % ( LANG_EN, LEVEL_KINDERGARTEN if idx < 10 else LEVEL_PRIMARY ) +
            ' ( "%s", %d );' % ( LANG_LO, LEVEL_KINDERGARTEN if idx < 10 else LEVEL_PRIMARY )
        )

        sentence_en_db_id = idx * 2 + 1
        sentence_lo_db_id = idx * 2 + 2

        db.execute(

            'INSERT INTO sentence_words( sentence, word, position ) VALUES ' +

                ','.join( [
                    '( %d, %d, %d )' % (
                        sentence_en_db_id,
                        wid,
                        pos )

                    for wid, pos in zip( sentence_words_en, range( len( sentence_words_en ) ) )
                ] ) + ',' +

                ','.join( [
                    '( %d, %d, %d )' % (
                        sentence_lo_db_id,
                        wid,
                        pos )

                    for wid, pos in zip( sentence_words_lo, range( len( sentence_words_lo ) ) )
                ] )
        )

        if idx > 1:

            db.execute(

                'INSERT INTO examples( sentence, word_position, meaning ) VALUES' +
                ' ( %d, %d, %d ),' % ( sentence_en_db_id, 2, wm_wo_en_db_id ) +
                ' ( %d, %d, %d );' % ( sentence_lo_db_id, 3, wm_wo_lo_db_id )
            )


#############################################################################
#	REINITIALIZE OPENWORDER DATABASE
#
#	- make a backup of the current database before reset
#	- initialize SQL schema ( tables )
#	- insert default data ( words, lists, ... )
#
if __name__ == '__main__':

    data_file = 'openworder.db'

    try:

        os.rename( data_file, data_file + '.bak' )

    except OSError:

        pass

    db = sqlite3.connect( data_file )

    init_schema ( db )
    init_players( db )
    init_data   ( db )

    db.commit()
